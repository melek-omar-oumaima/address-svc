# Use an official OpenJDK runtime as a parent image
FROM openjdk:8-jre-alpine

# Set the working directory to /app
WORKDIR /app

# Copy the application JAR file into the container at /app
COPY target/address-service-0.0.1-SNAPSHOT.jar /app/address-service-0.0.1-SNAPSHOT.jar

# Expose port 8080 to the outside world
EXPOSE 8083

# Define environment variables
ENV JAVA_OPTS=""

# Run the application when the container starts
CMD ["java", "-jar", "address-service-0.0.1-SNAPSHOT.jar"]

